#author: Kristian K Ullrich
#date: November 2017
#email: ullrich@evolbio.mpg.de

TMP_DIR <- "/tmp"

SEQ_FILE <- "http://wwwuser.gwdg.de/~evolbio/evolgen/wildmouse/introgression/ngm/fasta/ind/chr3.ngm.minQ13.uniqueOnly.setMinDepthInd5.setMaxDepthInd100.fa"
chr <- "chr3"

WSIZE <- 25000
WJUMP <- 25000

OUT_FILE <- paste0(TMP_DIR,"/",chr,".sw",WSIZE,".","wj",WJUMP,".newick.trees")

#####
library(distIUPAC)
library(ape)
source("https://gitlab.gwdg.de/evolgen/introgression/raw/master/scripts/sliding_window_steps_generator.r")
options(scipen=22)

#####MAIN#####

sink(OUT_FILE)
cat("scaffold\tstart\tend\tmid\tsites\ttree\n")
sink(NULL)

tmp.chr.seq<-readBStringSet(SEQ_FILE)
tmp.sw<-sliding_window_steps_generator(window=WSIZE,jump=WJUMP,start.by=1,end.by=unique(width(tmp.chr.seq)))
for(i in 1:dim(tmp.sw)[2]){
    tmp.out<-subseq(tmp.chr.seq,tmp.sw[1,i],tmp.sw[2,i])
    tmp.window.len<-(tmp.sw[2,i]-tmp.sw[1,i]+1)[[1]]
    tmp.complete.deletion.len<-length(which(unlist(apply(as.matrix(tmp.out),2,function(x) all(x=="N")))))
    if(tmp.complete.deletion.len/tmp.window.len>0.5){
        cat(c(chr,tmp.sw[,i],tmp.window.len-tmp.complete.deletion.len,NA),sep="\t",file=OUT_FILE,append=TRUE)
        cat("\n",file=OUT_FILE,append=TRUE)
        next()
    }
    tmp.out.dist<-distIUPAC(as.character(tmp.out))
    colnames(tmp.out.dist)<-rownames(tmp.out.dist)<-gsub("-","_",unlist(lapply(strsplit(names(tmp.out),"\\."),function(x) paste0(x[1],"_",x[2]))))
    tmp.out.dist.bionjs<-bionjs(as.dist(tmp.out.dist))
    tmp.tree<-write.tree(tmp.out.dist.bionjs)
    cat(c(chr,tmp.sw[,i],tmp.window.len-tmp.complete.deletion.len,tmp.tree),sep="\t",file=OUT_FILE,append=TRUE)
    cat("\n",file=OUT_FILE,append=TRUE)
}

#####

quit()

