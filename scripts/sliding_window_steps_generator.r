#author: Kristian K Ullrich
#date: July 2017
#email: ullrich@evolbio.mpg.de

sliding_window_steps_generator = function(window=10000,jump=1000,start.by=1,end.by=20000){
    if(end.by<=start.by){
        stop("end.by <= start.by")
    }
    start.seq = seq(start.by, end.by, by = jump)
    end.seq = seq(start.by + window - 1, end.by, by = jump)
    end.seq = c(end.seq, rep(end.by, length(start.seq)-length(end.seq)))
    start.end.matrix = rbind(start.seq, end.seq)
    start.end.matrix = rbind(start.end.matrix, apply(start.end.matrix, 2, function(x) {mean( c( x[1] - 1, x[2]) )} ) )
    start.end.matrix[2,]<-as.numeric(sprintf("%.0f",start.end.matrix[2,]))
    row.names(start.end.matrix) = c("start", "end", "mid")
    return(start.end.matrix)
}

