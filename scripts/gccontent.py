#!/usr/bin/python
# -*- coding: UTF-8 -*-

'''
author: Kristian K Ullrich
date: July 2017
email: ullrich@evolbio.mpg.de
License: MIT

The MIT License (MIT)

Copyright (c) 2017 Kristian K Ullrich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

This software relies on the following python packages:
sys
os
argparse
numpy https://pypi.python.org/pypi/numpy
Bio https://github.com/biopython/DIST
'''


import sys
import os
import argparse
from Bio import SeqIO
import numpy as np
from Bio.SeqUtils import GC


def sliding_window_steps_generator(sw,j,start,end):
    if end<=start:
        print 'end must be smaller than start'
        return
    start_seq = np.arange(start,end,j)
    end_seq = np.arange(start+sw-1,end,j)
    end_seq = np.append(end_seq,np.repeat(end,len(start_seq)-len(end_seq)))
    mid_seq = (start_seq+end_seq)/2
    return([start_seq,end_seq])


#main
def main():
    parser = argparse.ArgumentParser(prog='gccontent', usage='%(prog)s [options] [<arguments>...]',
                                     description='script to extract GC content from nucleotide fasta file')
    parser.add_argument('-i', help='specify FASTA input file')
    parser.add_argument('-o', help='specify input file')
    parser.add_argument('-w', help='specify sliding window size [default:25000]', default = '25000', type = int)
    parser.add_argument('-j', help='specify sliding window jump [default:25000]', default = '25000', type = int)
    args = parser.parse_args()
    if args.i is None:
        parser.print_help()
        sys.exit('\nPlease specify FASTA input file')
    args.i = os.path.abspath(args.i)
    if args.o is None:
        parser.print_help()
        sys.exit('\nPlease specify output file')
    args.o = os.path.abspath(args.o)
    print args
    fasta = SeqIO.parse(args.i,'fasta')
    with open(args.o,'w') as outhandle:
        for r in fasta:
            tmp_windows_start, tmp_windows_end = sliding_window_steps_generator(args.w, args.j, 1, len(r.seq))
            for s,e in zip(tmp_windows_start, tmp_windows_end):
                tmp_N_count = r.seq[s:e].count('N')
                tmp_N_percent = float(tmp_N_count) / float(len(r.seq[s:e]))
                if tmp_N_percent==1.0:
                    tmp_GC_percent = 0.0
                if tmp_N_percent!=1.0:
                    tmp_G_count = r.seq[s:e].count('G')
                    tmp_C_count = r.seq[s:e].count('C')
                    tmp_A_count = r.seq[s:e].count('A')
                    tmp_T_count = r.seq[s:e].count('T')
                    tmp_GC_percent = (float(tmp_G_count) + float(tmp_C_count)) / (float(len(r.seq[s:e])) - float(tmp_N_count))
                outhandle.write('%s\t%i\t%i\t%f\t%f\n' % (r.id,s,e,tmp_N_percent,tmp_GC_percent))


if __name__ == '__main__':
    main()

